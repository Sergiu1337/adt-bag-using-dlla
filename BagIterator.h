#include "Bag.h"

class BagIterator
{
	//DO NOT CHANGE THIS PART
	friend class Bag;
	
private:
	const Bag& bag;
	//TODO  - Representation
	BagIterator(const Bag& b);

	int currentNode;
	int currentFrequency;
public:
	void first();
	void next();
	TElem getCurrent() const;
	bool valid() const;
};
