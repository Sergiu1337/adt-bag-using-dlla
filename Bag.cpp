#include "Bag.h"
#include "BagIterator.h"
#include <exception>
#include <iostream>
using namespace std;

Bag::Bag() {
	this->head = -1;
	this->tail = -1;
    this->elements = new DLLANode[1];
    this->elementsArraySize = 0;
    this->elementsArrayCapacity = 1;
    this->firstFreePosition = -1;
}
// Theta(1)

void Bag::add(TElem e) {
    if (needsReadjustment()) {
        readjustArray();
    }
    int currentNode = head;
    while (currentNode != -1 && elements[currentNode].elements != e) {
        currentNode = elements[currentNode].next;
    }
    if (currentNode == -1) {
        ++elementsArraySize;
        if (head == -1) {
            int nextFreePosition = elements[firstFreePosition].next;
            head = firstFreePosition;
            tail = firstFreePosition;
            elements[head].elements = e;
            elements[head].frequency = 1;
            elements[head].next = -1;
            elements[head].prev  = -1;
            firstFreePosition = nextFreePosition;
        }
        else {
            int nextFreePosition = elements[firstFreePosition].next;
            elements[firstFreePosition].elements = e;
            elements[firstFreePosition].frequency = 1;
            elements[firstFreePosition].next = -1;
            elements[firstFreePosition].prev = tail;
            elements[tail].next = firstFreePosition;
            tail = firstFreePosition;
            firstFreePosition = nextFreePosition;
        }
    }
    else {
        int frequency = elements[currentNode].frequency;
        elements[currentNode].frequency = frequency + 1;
    }
}
// Best case :Theta(1) Worst Case:Theta(n) => Total complexity : O(n)


bool Bag::remove(TElem elem) {
    int currentNode = head;

    while (currentNode != -1 && elements[currentNode].elements != elem) {
        currentNode = elements[currentNode].next;
    }
    if (currentNode == -1)
        return false;

    int frequency = elements[currentNode].frequency;
    if (frequency > 1) {
        elements[currentNode].frequency = frequency - 1;
    }
    else {
        --elementsArraySize;
        int prevNode = elements[currentNode].prev;
        int nextNode = elements[currentNode].next;
        if (currentNode == tail) {
            tail = prevNode;
            if (tail == -1) {
                head = -1;
            }
        }
        else if (currentNode == head) {
            head = nextNode;
            if (head == -1) {
                tail = -1;
            }
        }
        elements[currentNode].next = firstFreePosition;
        firstFreePosition = currentNode;
        if (prevNode != -1) {
            elements[prevNode].next = nextNode;
        }
        if (nextNode != -1) {
            elements[nextNode].prev = prevNode;
        }
    }
    return true;
}
// Best case :Theta(1) Worst Case:Theta(n) => Total complexity : O(n)

bool Bag::search(TElem elem) const {
    int currentNode = head;
    while (currentNode != -1 && elements[currentNode].elements != elem) {
        currentNode = elements[currentNode].next;
    }
    return currentNode != -1;
}
// Best case :Theta(1) Worst Case:Theta(n) => Total complexity : O(n)

int Bag::nrOccurrences(TElem elem) const {
    int currentNode = head;
    while (currentNode != -1) {
        if (elements[currentNode].elements == elem) {
            return elements[currentNode].frequency;
        }
        currentNode = elements[currentNode].next;
    }
    return 0;
}
// Best case :Theta(1) Worst Case:Theta(n) => Total complexity : O(n)

int Bag::size() const {
    int sz = 0;
    int currentNode = head;
    while (currentNode != -1) {
        sz += elements[currentNode].frequency;
        currentNode = elements[currentNode].next;
    }
    return sz;
}
// Best case :Theta(1) Worst Case:Theta(n) => Total complexity : O(n)


bool Bag::isEmpty() const {
    return elementsArraySize == 0;
}
//Theta(1)

BagIterator Bag::iterator() const {
	return BagIterator(*this);
}
//Theta(1)
Bag::~Bag() {
    delete[] elements;
}
//Theta(1)
bool Bag::needsReadjustment() const {
    return firstFreePosition == -1;
}
//Theta(1)
void Bag::readjustArray() {
    DLLANode* newElements = new DLLANode[2 * elementsArrayCapacity];
    for (int i = 0; i < elementsArrayCapacity; ++i) {
        newElements[i] = elements[i];
    }
    for (int i = elementsArrayCapacity; i < elementsArrayCapacity * 2 - 1; ++i) {
        if (i > elementsArrayCapacity) {
            newElements[i].prev = i - 1;
        }
        newElements[i].next = i + 1;
    }
    newElements[elementsArrayCapacity * 2 - 1].next = -1;
    delete[] elements;
    elements = newElements;
    firstFreePosition = elementsArrayCapacity;
    elementsArrayCapacity *= 2;
}
// Best case :Theta(1) Worst Case:Theta(elements) => Total complexity : O(n)

int Bag::elementsWithMinimumFrequency() const
{
    if (isEmpty()) {
        return 0;
    }
    else {
        int count = 0;
        int minimumFrequency = elements[head].frequency;
        int currentNode = head;
        while (elements[currentNode].next != -1) {
            currentNode = elements[currentNode].next;
            int actualFrequency = elements[currentNode].frequency;
            if (minimumFrequency > actualFrequency) {
                minimumFrequency = actualFrequency;
            }
        }
        currentNode = head;
        while (currentNode != -1) {
            int actualFrequency = elements[currentNode].frequency;
            if (actualFrequency == minimumFrequency) {
                count++;
            }
            currentNode = elements[currentNode].next;
        }
        return count;
    }
    
}
// Theta(2n) = Theta(n)
