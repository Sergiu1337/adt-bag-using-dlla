#include <exception>
#include "BagIterator.h"
#include "Bag.h"

#include<exception>

using namespace std;


BagIterator::BagIterator(const Bag& c): bag(c)
{
    first();
}
// Theta(1)

void BagIterator::first() {
    if (bag.elementsArraySize > 0) {
        currentNode = bag.head;
        currentFrequency = bag.elements[bag.head].frequency;
    }
    else {
        currentNode = -1;
    }
}
// Theta(1)

void BagIterator::next() {
    if (!valid()) {
        throw exception();
    }
    --currentFrequency;
    if (currentFrequency == 0) {
        currentNode = bag.elements[currentNode].next;
        if (currentNode != -1) {
            currentFrequency = bag.elements[currentNode].frequency;
        }
    }
}
// Theta(1)

bool BagIterator::valid() const {
    return currentNode != -1;
}
// Theta(1)
TElem BagIterator::getCurrent() const
{
    if (!valid()) {
        throw exception();
    }
    return bag.elements[currentNode].elements;
}
// Theta(1)